### COAKT Manifesto

### COAKT...Act Together

#### what we believe:

All persons possess a special talent, even if, in this moment, they don’t know what it is. 
All persons have the right to dream, no matter their circumstances.
Money is necessary for survival, but it doesn’t equal abundance.
Abundance is a spiritual, physical, and mental state.
It’s never too late to dream and manifest that dream.
Every individual has the right to control their destiny.
The definition of talent needs to be rewritten.A
ll individuals have the right to commerce, to trade skills and services freely.
Prosperity should be shared.
Togetherness is the key to leveling the playing field and revolutionizing our collective mindset.

#### what is the problem?

The problem is a flawed understanding of what skills, talent, and assets really are. 
We’ve been led to believe that all skills are not created equal. 
This belief has kept us separate and stuck. 
We’ve been led to believe that talent is only a natural-born gift or the result of an education that’s not accessible to all. 
We falsely believe we can’t escape the pay-to-play system and we remain locked out and disenfranchised. 
We feel that there’s not enough. 
Other people are a hindrance rather than essential to our growth. 
We think that giving means creating less for ourselves. 
We resent the success of others. 
The problem is our belief that more money is the key to our wellbeing.

#### how do we fix it?

We create a forum that reshapes and broadens our understanding of assets. 
We help uncover your dreams and desires. 
We give you tools to allow your talents emerge. 
We help you open up. 
We create a platform that trades value for value. 
We facilitate connections and trades between people who would have never found one another.
We help you thrive by being of service to others. 
We rethink what reliance on other people really means. 

#### what is the end result?

Sustained prosperity.
True fairness. 
Equality, trust, and camaraderie. 
A system that works. 
Inner peace and abundance